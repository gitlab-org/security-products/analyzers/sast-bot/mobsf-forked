package main

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"

	"gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var findings []*Finding
	if err := json.NewDecoder(reader).Decode(&findings); err != nil {
		return nil, err
	}

	var vulns []report.Vulnerability
	for _, f := range findings {
		// The info findings are extremely high-volume and low-value.
		// So, we won't report them.
		if f.SeverityLevel() <= report.SeverityLevelInfo {
			continue
		}

		for _, loc := range f.Locations {
			v := report.Vulnerability{
				Category:    report.CategorySast,
				Scanner:     &metadata.VulnerabilityScanner,
				Name:        f.ID,
				Description: f.Description,
				Severity:    f.SeverityLevel(),
				Location:    loc.Location(),
				Identifiers: []report.Identifier{
					{
						Type:  "mobsf_rule_id",
						Name:  fmt.Sprintf("MobSF Rule ID %s", f.ID),
						Value: f.ID,
					},
				},
			}
			vulns = append(vulns, v)
		}
	}

	// Temporary until we improve sorting in the report package.
	sort.SliceStable(vulns, func(i, j int) bool {
		v1, v2 := vulns[i], vulns[j]
		return v1.Identifiers[0].Name < v2.Identifiers[0].Name
	})

	sort.SliceStable(vulns, func(i, j int) bool {
		v1, v2 := vulns[i], vulns[j]
		return fmt.Sprintf("%s:%d", v1.Location.File, v1.Location.LineStart) < fmt.Sprintf("%s:%d", v2.Location.File, v2.Location.LineStart)
	})

	r := report.NewReport()
	r.Version = report.Version{
		Major: 15,
		Minor: 0,
		Patch: 7,
	}
	r.Analyzer = metadata.AnalyzerID
	r.Config.Path = ruleset.PathSAST
	r.Vulnerabilities = vulns
	r.Scan.Scanner = metadata.ReportScanner
	return &r, nil
}
