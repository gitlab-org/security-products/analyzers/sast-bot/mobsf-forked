package plugin_test

import (
	"path/filepath"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2/plugin"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/search"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/walk"
)

func TestMatch(t *testing.T) {
	base := "../qa/fixtures"
	tests := []struct {
		Name          string
		ProjectRoot   string
		ExpectedMatch string
	}{
		{
			Name:          "when a repository containing an AndroidManifest.xml file in the Android Studio location is found",
			ProjectRoot:   filepath.Join(base, "java-android"),
			ExpectedMatch: filepath.Join(base, "java-android/app/src/main"),
		},
		{
			Name:          "when a repository containing an AndroidManifest.xml file in the Eclipse location is found",
			ProjectRoot:   filepath.Join(base, "java-android-eclipse"),
			ExpectedMatch: filepath.Join(base, "java-android-eclipse"),
		},
		{
			Name:          "when a repository containing an .xcodeproj is found",
			ProjectRoot:   filepath.Join(base, "swift-ios"),
			ExpectedMatch: filepath.Join(base, "swift-ios/App.xcodeproj"), // .xcodeproj is a dir
		},
		{
			Name:          "when a repository containing an .apk is found",
			ProjectRoot:   filepath.Join(base, "java-android-apk"),
			ExpectedMatch: filepath.Join(base, "java-android-apk"),
		},
	}

	for _, tc := range tests {
		t.Run(tc.Name, func(t *testing.T) {
			actualMatch, err := search.New(plugin.Match, &walk.Options{MaxDepth: 20}).Run(tc.ProjectRoot)
			require.NoError(t, err)

			assert.Equal(t, tc.ExpectedMatch, actualMatch)
		})
	}
}
