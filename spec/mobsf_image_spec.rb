require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def target_mount_dir
    '/app'
  end

  def image_name
    ENV.fetch('TMP_IMAGE', 'mobsf:latest')
  end

  context 'with no project' do
    before(:context) do
      @output = `docker run -t --rm -w #{target_mount_dir} -e CI_PROJECT_DIR=#{target_mount_dir} #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it 'shows there is no match' do
      expect(@output).to match(/no match in #{target_mount_dir}/i)
    end

    describe 'exit code' do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = "gl-sast-report.json")
      path = File.join(expectations_dir, expectation_name, report_name)
      if ENV['REFRESH_EXPECTED'] == "true"
        # overwrite the expected JSON with the newly generated JSON
        FileUtils.cp(scan.report_path, File.expand_path(path))
      end
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',

        # CI_PROJECT_DIR env var value is referred by the Analyzer(1) and
        # Post Analyzer script(2) to determine the target directory for
        # performing any action.
        #
        # References:
        # (1): https://gitlab.com/gitlab-org/security-products/analyzers/command/blob/6eb84053950dcf4c06b768484880f9d3d9aebde1/run.go#L132-132
        # (2): https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': target_mount_dir,
        'MOBSF_API_KEY': 'key',
        'SEARCH_MAX_DEPTH': '20'
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context "with android application source files" do
      let(:project) { "java-android" }

      context "by default" do
        it_behaves_like "successful scan"
        it_behaves_like "non-empty report"
        it_behaves_like "valid report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end
      end

      context "with tracking enabled" do
          let(:variables) do
            { 'GITLAB_FEATURES': 'vulnerability_finding_signatures' }
          end
  
          it_behaves_like "successful scan"
          it_behaves_like "non-empty report"
          it_behaves_like "valid report"
  
          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project, 'gl-sast-report-with-tracking.json') }
          end
      end
    end

    context "with android application with multiple modules" do
      let(:project) { "java-android-multimodule" }

      it_behaves_like "successful scan"
      it_behaves_like "non-empty report"
      it_behaves_like "valid report"

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse_expected_report(project) }
      end
    end

    context "with android application .apk file" do
      let(:project) { "java-android-apk" }

      it_behaves_like "successful scan"
      it_behaves_like "non-empty report"
      it_behaves_like "valid report"

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse_expected_report(project) }
      end
    end

    context "with everything but the kitchen sink" do
      let(:project) { "kitchen-sink" }

      it_behaves_like "successful scan"
      it_behaves_like "non-empty report"
      it_behaves_like "valid report"

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse_expected_report(project) }
      end
    end

    context "with an Android Eclipse project" do
      let(:project) { "java-android-eclipse" }

      it_behaves_like "successful scan"
      it_behaves_like "non-empty report"
      it_behaves_like "valid report"

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse_expected_report(project) }
      end
    end

    context "with a nested Android Studio project" do
      let(:project) { "java-android-nested" }

      it_behaves_like "successful scan"
      it_behaves_like "non-empty report"
      it_behaves_like "valid report"

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse_expected_report(project) }
      end
    end

    context "with an iOS project" do
      let(:project) { "swift-ios" }

      it_behaves_like "successful scan"
      it_behaves_like "non-empty report"
      it_behaves_like "valid report"

      it_behaves_like "recorded report" do
        let(:recorded_report) { parse_expected_report(project) }
      end
    end

    context "with an Android project containing empty entrypoints" do
      let(:project) { "java-android-bug-423542" }

      it_behaves_like "successful scan"
      it_behaves_like "empty report"
      it_behaves_like "valid report"
    end
  end
end
